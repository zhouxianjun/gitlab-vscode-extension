import vscode from 'vscode';
import {
  SUGGESTION_ACCEPTED_COMMAND,
  DidChangeDocumentInActiveEditor,
} from '@gitlab-org/gitlab-lsp';
import { LanguageClientFactory } from './client_factory';
import { getClientContext } from './get_client_context';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { CONFIG_NAMESPACE } from '../constants';
import { LanguageClientWrapper } from './language_client_wrapper';
import { CodeSuggestionsStateManager } from '../code_suggestions/code_suggestions_state_manager';
import { CodeSuggestionsStatusBarItem } from '../code_suggestions/code_suggestions_status_bar_item';
import { CodeSuggestionsGutterIcon } from '../code_suggestions/code_suggestions_gutter_icon';
import { LanguageClientMiddleware } from './language_client_middleware';
import {
  COMMAND_TOGGLE_CODE_SUGGESTIONS,
  toggleCodeSuggestions,
} from '../code_suggestions/commands/toggle';
import {
  CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
  codeSuggestionStreamAccepted,
} from '../code_suggestions/commands/code_suggestion_stream_accepted';
import { GitLabPlatformManagerForCodeSuggestions } from '../code_suggestions/gitlab_platform_manager_for_code_suggestions';

export const registerLanguageServer = async (
  context: vscode.ExtensionContext,
  clientFactory: LanguageClientFactory,
  gitlabPlatformManager: GitLabPlatformManager,
) => {
  const stateManager = new CodeSuggestionsStateManager(gitlabPlatformManager, context);
  const statusBarItem = new CodeSuggestionsStatusBarItem(stateManager);
  const gutterIcon = new CodeSuggestionsGutterIcon(context, stateManager);
  const middleware = new LanguageClientMiddleware(stateManager);
  const baseAssetsUrl = vscode.Uri.joinPath(
    context.extensionUri,
    './assets/language-server/',
  ).toString();

  const client = clientFactory.createLanguageClient(context, {
    documentSelector: [{ scheme: 'file' }],
    initializationOptions: {
      ...getClientContext(),
      baseAssetsUrl,
    },
    middleware,
  });
  middleware.client = client;
  stateManager.client = client;
  await stateManager.init();
  const suggestionsManager = new GitLabPlatformManagerForCodeSuggestions(gitlabPlatformManager);
  const wrapper = new LanguageClientWrapper(client, suggestionsManager, stateManager);
  await wrapper.initAndStart();
  context.subscriptions.push(
    suggestionsManager,
    wrapper,
    vscode.commands.registerCommand(
      SUGGESTION_ACCEPTED_COMMAND,
      wrapper.sendSuggestionAcceptedEvent,
    ),
    vscode.commands.registerCommand(COMMAND_TOGGLE_CODE_SUGGESTIONS, () =>
      toggleCodeSuggestions({ stateManager }),
    ),
    vscode.commands.registerCommand(
      CODE_SUGGESTION_STREAM_ACCEPTED_COMMAND,
      codeSuggestionStreamAccepted(client),
    ),
    vscode.workspace.onDidChangeConfiguration(async e => {
      if (!e.affectsConfiguration(CONFIG_NAMESPACE)) {
        return;
      }

      await wrapper.syncConfig();
    }),
    gitlabPlatformManager.onAccountChange(wrapper.syncConfig),
    gitlabPlatformManager.onAccountChange(wrapper.syncConfig),
    statusBarItem,
    gutterIcon,
    vscode.window.onDidChangeActiveTextEditor(async te => {
      if (te) {
        await client.sendNotification(DidChangeDocumentInActiveEditor, {
          ...te.document,
          uri: te.document.uri.toString(),
        });
      }
    }),
  );
};
