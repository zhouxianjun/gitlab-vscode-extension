import * as vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import * as featureFlags from '../feature_flags';
import {
  CodeSuggestionsStateManager,
  VisibleCodeSuggestionsState,
} from './code_suggestions_state_manager';
import {
  createConfigurationChangeTrigger,
  createFakeWorkspaceConfiguration,
} from '../test_utils/vscode_fakes';
import { createFakePartial } from '../test_utils/create_fake_partial';
import { GitLabPlatformManager } from '../platform/gitlab_platform';
import { GitLabPlatformManagerForCodeSuggestions } from './gitlab_platform_manager_for_code_suggestions';
import { createExtensionContext, gitlabPlatformForProject } from '../test_utils/entities';
import { disabledForSessionPolicy } from './state_policy/disabled_for_session_policy';
import { MinimumGitLabVersionPolicy } from './state_policy/minimal_gitlab_version_policy';
import { StatePolicy } from './state_policy/state_policy';
import { createFakePolicy } from './state_policy/test_utils/create_fake_policy';
import { LicenseStatusPolicy } from './state_policy/license_status_policy';
import { ProjectDisabledPolicy } from './state_policy/project_disabled_policy';
import { MissingAccountPolicy } from './state_policy/missing_account_policy';
import { SupportedLanguagePolicy } from './state_policy/supported_language_policy';
import { LanguageServerPolicy } from './state_policy/language_server_policy';

jest.mock('./gitlab_platform_manager_for_code_suggestions');
jest.mock('../feature_flags');
jest.mock('./state_policy/license_status_policy');
jest.mock('./state_policy/project_disabled_policy.ts');
jest.mock('./state_policy/minimal_gitlab_version_policy.ts');
jest.mock('./state_policy/supported_language_policy.ts');
jest.mock('./state_policy/language_server_policy.ts');
jest.mock('./state_policy/missing_account_policy.ts');
jest.useFakeTimers();

describe('Code suggestions state manager', () => {
  let triggerSettingsRefresh: () => void;
  let stateManager: CodeSuggestionsStateManager;
  let platformManager: GitLabPlatformManager;
  let suggestionsPlatformManager: GitLabPlatformManagerForCodeSuggestions;
  let licensePolicyMock: StatePolicy;
  let projectDisabledPolicyMock: StatePolicy;
  let minimalGitLabVersionPolicyMock: StatePolicy;
  let supportedLanguagePolicyMock: StatePolicy;
  let languageServerPolicyMock: StatePolicy;
  let missingAccountPolicyMock: StatePolicy;
  let client: BaseLanguageClient;
  const context = createExtensionContext();
  let mockedPolicies: StatePolicy[] = [];

  beforeEach(async () => {
    licensePolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/license_status_policy').NO_LICENSE,
    );
    projectDisabledPolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/project_disabled_policy').DISABLED_BY_PROJECT,
    );
    minimalGitLabVersionPolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/minimal_gitlab_version_policy').UNSUPPORTED_GITLAB_VERSION,
    );
    languageServerPolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/language_server_policy').UNSUPPORTED_LANGUAGE,
    );
    jest.mocked(LicenseStatusPolicy).mockReturnValue(licensePolicyMock as LicenseStatusPolicy);

    supportedLanguagePolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/supported_language_policy').UNSUPPORTED_LANGUAGE,
    );

    missingAccountPolicyMock = createFakePolicy(
      jest.requireActual('./state_policy/missing_account_policy').NO_ACCOUNT,
    );

    jest
      .mocked(ProjectDisabledPolicy)
      .mockReturnValue(projectDisabledPolicyMock as ProjectDisabledPolicy);

    jest
      .mocked(MinimumGitLabVersionPolicy)
      .mockReturnValue(minimalGitLabVersionPolicyMock as MinimumGitLabVersionPolicy);

    jest
      .mocked(MissingAccountPolicy)
      .mockReturnValue(missingAccountPolicyMock as MissingAccountPolicy);

    jest
      .mocked(SupportedLanguagePolicy)
      .mockReturnValue(supportedLanguagePolicyMock as SupportedLanguagePolicy);

    jest
      .mocked(LanguageServerPolicy)
      .mockReturnValue(languageServerPolicyMock as LanguageServerPolicy);

    jest.mocked(featureFlags.isEnabled).mockReturnValue(true);

    // these triggers need to be created BEFORE state manager adds listeners to VS Code API
    triggerSettingsRefresh = createConfigurationChangeTrigger();

    // ensure the suggestions are enabled in settings
    jest
      .mocked(vscode.workspace.getConfiguration)
      .mockReturnValue(createFakeWorkspaceConfiguration({ enabled: true }));

    // pretend there is a GitLab account
    suggestionsPlatformManager = createFakePartial<GitLabPlatformManagerForCodeSuggestions>({
      getGitLabPlatform: jest.fn().mockResolvedValue(gitlabPlatformForProject),
      dispose: () => {},
    });
    jest
      .mocked(GitLabPlatformManagerForCodeSuggestions)
      .mockReturnValue(suggestionsPlatformManager);

    mockedPolicies = [
      licensePolicyMock,
      projectDisabledPolicyMock,
      minimalGitLabVersionPolicyMock,
      languageServerPolicyMock,
      supportedLanguagePolicyMock,
      minimalGitLabVersionPolicyMock,
      missingAccountPolicyMock,
    ];

    client = createFakePartial<BaseLanguageClient>({});
    stateManager = new CodeSuggestionsStateManager(platformManager, context);
    disabledForSessionPolicy.setTemporaryDisabled(false);
    await stateManager.init();
  });

  afterEach(() => {
    stateManager.dispose();
  });

  const disableViaSettings = () => {
    jest
      .mocked(vscode.workspace.getConfiguration)
      .mockReturnValue(createFakeWorkspaceConfiguration({ enabled: false }));
    triggerSettingsRefresh();
  };

  describe('constructor', () => {
    it('reads settings and sets the disabled in settings property', () => {
      jest
        .mocked(vscode.workspace.getConfiguration)
        .mockReturnValue(createFakeWorkspaceConfiguration({ enabled: false }));

      stateManager = new CodeSuggestionsStateManager(platformManager, context);

      expect(stateManager.getVisibleState()).toBe(
        VisibleCodeSuggestionsState.DISABLED_VIA_SETTINGS,
      );
    });
  });

  describe('init', () => {
    it('if a policy initialization fails, the init does not crash', async () => {
      licensePolicyMock.init = jest.fn().mockRejectedValue(new Error('init error'));

      stateManager = new CodeSuggestionsStateManager(platformManager, context, client);

      await expect(stateManager.init()).resolves.not.toThrow();
    });
  });

  describe('visible state', () => {
    type StateMutation = () => void | Promise<void>;
    const mutationsFromLeastImportant: {
      mutation: StateMutation;
      expectedState: VisibleCodeSuggestionsState;
    }[] = [
      {
        mutation: () => stateManager.setLoading(true),
        expectedState: VisibleCodeSuggestionsState.LOADING,
      },
      {
        mutation: () => stateManager.setError(true),
        expectedState: VisibleCodeSuggestionsState.ERROR,
      },
      {
        mutation: () => {
          projectDisabledPolicyMock.engaged = true;
          jest.mocked(projectDisabledPolicyMock.onEngagedChange).mock.calls[0][0](true);
        },
        expectedState: VisibleCodeSuggestionsState.DISABLED_BY_PROJECT,
      },
      {
        mutation: () => {
          licensePolicyMock.engaged = true;
          jest.mocked(licensePolicyMock.onEngagedChange).mock.calls[0][0](true);
        },
        expectedState: VisibleCodeSuggestionsState.NO_LICENSE,
      },
      {
        mutation: () => {
          minimalGitLabVersionPolicyMock.engaged = true;
          jest.mocked(minimalGitLabVersionPolicyMock.onEngagedChange).mock.calls[0][0](true);
        },
        expectedState: VisibleCodeSuggestionsState.UNSUPPORTED_GITLAB_VERSION,
      },
      {
        mutation: async () => {
          missingAccountPolicyMock.engaged = true;
          jest.mocked(missingAccountPolicyMock.onEngagedChange).mock.calls[1][0](true);
        },
        expectedState: VisibleCodeSuggestionsState.NO_ACCOUNT,
      },
      {
        mutation: () => disabledForSessionPolicy.setTemporaryDisabled(true),
        expectedState: VisibleCodeSuggestionsState.DISABLED_BY_USER,
      },
      {
        mutation: disableViaSettings,
        expectedState: VisibleCodeSuggestionsState.DISABLED_VIA_SETTINGS,
      },
    ];

    const supportedLanguageClientPolicy = {
      mutation: async () => {
        supportedLanguagePolicyMock.engaged = true;
        jest.mocked(supportedLanguagePolicyMock.onEngagedChange).mock.calls[0][0](true);
      },
      expectedState: VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE,
    };
    const lsPolicy = {
      mutation: async () => {
        languageServerPolicyMock.engaged = true;
        jest.mocked(languageServerPolicyMock.onEngagedChange).mock.calls[0][0](true);
      },
      expectedState: VisibleCodeSuggestionsState.UNSUPPORTED_LANGUAGE,
    };

    describe.each`
      enabled  | msg
      ${true}  | ${'Language Server feature flag is enabled'}
      ${false} | ${'Language Server feature flag is disabled'}
    `('when $msg', ({ enabled }) => {
      let mutations: {
        mutation: StateMutation;
        expectedState: VisibleCodeSuggestionsState;
      }[];

      beforeEach(async () => {
        mockedPolicies.forEach(policy => {
          jest.mocked(policy).onEngagedChange.mockClear();
        });
        const clientOrLSPolicy = enabled ? lsPolicy : supportedLanguageClientPolicy;
        const lsClient = enabled ? client : undefined;

        mutations = [...mutationsFromLeastImportant];
        mutations.splice(2, 0, clientOrLSPolicy);
        stateManager = new CodeSuggestionsStateManager(platformManager, context, lsClient);
        await stateManager.init();
      });

      it('more important state takes precedence over less important state', async () => {
        expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.READY);

        for (const scenario of mutations) {
          // we want to ensure that we execute the mutations in series
          // eslint-disable-next-line no-await-in-loop
          await scenario.mutation();
          expect(stateManager.getVisibleState()).toBe(scenario.expectedState);
        }
      });

      it('every state change triggers an event', async () => {
        const visibleStateChangeListener = jest.fn();
        stateManager.onDidChangeVisibleState(visibleStateChangeListener);
        for (const scenario of mutations) {
          visibleStateChangeListener.mockReset();
          // we want to ensure that we execute the mutations in series
          // eslint-disable-next-line no-await-in-loop
          await scenario.mutation();
          expect(visibleStateChangeListener).toHaveBeenCalledWith(scenario.expectedState);
        }
      });
    });
  });

  describe('Loading state', () => {
    it('handles parallel operations', () => {
      stateManager.setLoading(true);
      stateManager.setLoading(true);
      stateManager.setLoading(false);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.LOADING);

      stateManager.setLoading(false);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.READY);
    });

    it('can never enter negative loading', () => {
      stateManager.setLoading(true);
      stateManager.setLoading(false);
      stateManager.setLoading(false);
      stateManager.setLoading(false);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.READY);

      stateManager.setLoading(true);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.LOADING);
    });

    it('can associate loading with a resource', () => {
      const resource1 = {};
      const resource2 = {};

      stateManager.setLoadingResource(resource1, true);
      stateManager.setLoadingResource(resource1, true);
      stateManager.setLoadingResource(resource2, false);
      stateManager.setLoadingResource(resource2, false);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.LOADING);

      stateManager.setLoadingResource(resource1, false);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.READY);

      stateManager.setLoadingResource(resource2, true);

      expect(stateManager.getVisibleState()).toBe(VisibleCodeSuggestionsState.LOADING);
    });
  });

  describe('disabled & active', () => {
    let disabledByUserListener: jest.Func;

    beforeEach(async () => {
      disabledByUserListener = jest.fn();
      stateManager.onDidChangeDisabledByUserState(disabledByUserListener);
      await stateManager.init();
    });

    it('by default is not disabled and it is active', async () => {
      expect(stateManager.isDisabledByUser()).toBe(false);
      expect(stateManager.isActive()).toBe(true);
    });

    it('changes to disabled and inactive when disabling via settings', () => {
      disableViaSettings();

      expect(stateManager.isDisabledByUser()).toBe(true);
      expect(disabledByUserListener).toHaveBeenCalledWith(true);

      expect(stateManager.isActive()).toBe(false);
    });

    it('changes to disabled and inactive when disabling temporarily for session', () => {
      disabledForSessionPolicy.setTemporaryDisabled(true);

      expect(stateManager.isDisabledByUser()).toBe(true);
      expect(disabledByUserListener).toHaveBeenCalledWith(true);

      expect(stateManager.isActive()).toBe(false);
    });
  });
});
