import * as vscode from 'vscode';
import { CS_DISABLED_PROJECT_CHECK_INTERVAL } from '../constants';
import { getProjectCodeSuggestionsEnabled } from '../api/get_project_code_suggestions_enabled';
import { GitLabPlatform, GitLabPlatformForProject } from '../../platform/gitlab_platform';
import { log } from '../../log';
import { PassiveCache } from './passive_cache';
import { StatePolicy } from './state_policy';
import { GitLabPlatformManagerForCodeSuggestions } from '../gitlab_platform_manager_for_code_suggestions';
import { diffEmitter } from '../diff_emitter';

export const DISABLED_BY_PROJECT = 'code-suggestions-disabled-by-project';

/** can return undefined if the API call failed with something other than 403 error */
const isDisabledForProject = async (platform: GitLabPlatformForProject) => {
  const result = await platform.fetchFromApi(
    getProjectCodeSuggestionsEnabled(platform.project.namespaceWithPath),
  );
  return !result.project?.duoFeaturesEnabled;
};

export class ProjectDisabledPolicy implements StatePolicy {
  #subscriptions: vscode.Disposable[] = [];

  #isProjectDisabledCache = new PassiveCache<boolean>(CS_DISABLED_PROJECT_CHECK_INTERVAL);

  #eventEmitter = diffEmitter(new vscode.EventEmitter<boolean>());

  onEngagedChange = this.#eventEmitter.event;

  #manager: GitLabPlatformManagerForCodeSuggestions;

  #isDisabled = false;

  constructor(manager: GitLabPlatformManagerForCodeSuggestions) {
    this.#manager = manager;
    this.#subscriptions.push(
      this.#manager.onPlatformChange(async platform => {
        await this.#checkIfGitLabProjectDisabled(platform, vscode.window.activeTextEditor);
      }),
      vscode.window.onDidChangeActiveTextEditor(async te => {
        await this.#checkIfGitLabProjectDisabled(await this.#manager.getGitLabPlatform(), te);
      }),
    );
  }

  async init() {
    await this.#checkIfGitLabProjectDisabled(
      await this.#manager.getGitLabPlatform(),
      vscode.window.activeTextEditor,
    );
  }

  get engaged() {
    return this.#isDisabled;
  }

  state = DISABLED_BY_PROJECT;

  async #checkIfGitLabProjectDisabled(
    platform: GitLabPlatform | undefined,
    te?: vscode.TextEditor,
  ) {
    // abort if we don't have all the info
    if (!te || !platform?.project?.namespaceWithPath) {
      this.#isDisabled = false;
      this.#eventEmitter.fire(this.engaged);
      return;
    }

    // try cache
    const disabledCache = this.#isProjectDisabledCache.get(platform.project.namespaceWithPath);
    if (disabledCache !== undefined) {
      this.#isDisabled = disabledCache;
      this.#eventEmitter.fire(this.engaged);
      return;
    }

    try {
      const disabledInApi = await isDisabledForProject(platform);
      this.#isProjectDisabledCache.set(platform.project.namespaceWithPath, disabledInApi);
      this.#isDisabled = disabledInApi;
    } catch (e) {
      log.debug(
        `Checking if suggestions are disabled for the project ${platform?.project?.namespaceWithPath} failed with: `,
        e,
      );
      this.#isDisabled = false;
    }

    this.#eventEmitter.fire(this.engaged);
  }

  dispose() {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
