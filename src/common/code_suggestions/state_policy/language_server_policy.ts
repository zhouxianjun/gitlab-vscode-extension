import vscode from 'vscode';
import { BaseLanguageClient } from 'vscode-languageclient';
import {
  CodeSuggestionAvailabilityStateChange,
  CodeSuggestionsLSState,
} from '@gitlab-org/gitlab-lsp';
import { diffEmitter } from '../diff_emitter';
import { StatePolicy } from './state_policy';

export const UNSUPPORTED_LANGUAGE = 'code-suggestions-document-unsupported-language';

export class LanguageServerPolicy implements StatePolicy {
  #subscriptions: vscode.Disposable[] = [];

  #eventEmitter = diffEmitter(new vscode.EventEmitter<boolean>());

  #client: BaseLanguageClient;

  state: CodeSuggestionsLSState;

  engaged = false;

  constructor(client: BaseLanguageClient) {
    this.#client = client;
  }

  async init() {
    this.#client.onNotification(CodeSuggestionAvailabilityStateChange, state => {
      this.state = state;
      this.engaged = Boolean(state);
      this.#eventEmitter.fire(this.engaged);
    });
  }

  onEngagedChange = this.#eventEmitter.event;

  dispose(): void {
    this.#subscriptions.forEach(s => s.dispose());
  }
}
