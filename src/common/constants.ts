export const GITLAB_COM_URL = 'https://gitlab.com';
export const CONFIG_NAMESPACE = 'gitlab';

export const DO_NOT_SHOW_CODE_SUGGESTIONS_VERSION_WARNING = 'DO_NOT_SHOW_VERSION_WARNING';
// NOTE: This needs to _always_ be a 3 digits
export const MINIMUM_CODE_SUGGESTIONS_VERSION = '16.8.0';

export const DO_NOT_SHOW_VERSION_WARNING = 'DO_NOT_SHOW_VERSION_WARNING';

// NOTE: This needs to _always_ be a 3 digits
export const MINIMUM_VERSION = '16.1.0';
