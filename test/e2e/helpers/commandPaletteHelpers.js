import { browser } from '@wdio/globals';

/**
 * Waits for a given prompt to contain a given message.
 *
 * @async
 * @param {Object} prompt - The prompt object such as the return of workbench.executeCommand()
 * @param {string} message - The expected message text
 * @returns {Promise<void>}
 */
const waitForPromptToContain = async (prompt, message) => {
  await browser.waitUntil(
    async () => {
      const promptText = await prompt.getMessage();
      return promptText.includes(message);
    },
    {
      timeoutMsg: `Prompt text did not contain ${message}.`,
    },
  );
};

export { waitForPromptToContain };
