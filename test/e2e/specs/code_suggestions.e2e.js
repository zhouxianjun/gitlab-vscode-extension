import { browser } from '@wdio/globals';
import { completeAuth, generateRandomString, verifyCodeSuggestion } from '../helpers/index.js';

describe('GitLab Workflow Extension Code Suggestions', async () => {
  let workbench;
  let statusBar;
  let tab;

  before(async () => {
    await completeAuth();
  });

  beforeEach(async () => {
    workbench = await browser.getWorkbench();
    statusBar = await workbench.getStatusBar();
    const filename = `test-${generateRandomString(7)}.rb`;
    const prompt = await workbench.executeCommand('Create: New File...');

    await prompt.setText(filename);
    await prompt.confirm();
    await prompt.confirm();

    const editorView = await workbench.getEditorView();
    tab = await editorView.openEditor(filename);
  });

  it('suggests code after typing', async () => {
    const codePartial = `
    public class Vehicle {
      private String make;

      public String getMake() {
          return`;

    await tab.typeText(codePartial);

    await verifyCodeSuggestion(tab, statusBar, codePartial);
  });

  it('generates code given a prompt', async () => {
    const prompt = '# generate a simple hello world web server\n';

    await tab.setText(prompt);
    await browser.keys('Enter');

    await verifyCodeSuggestion(tab, statusBar, prompt);
  });
});
